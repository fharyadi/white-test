'use strict';

/**
 * @ngdoc overview
 * @name thewhiteApp
 * @description
 * # thewhiteApp
 *
 * Main module of the application.
 */
angular
    .module('thewhiteApp', [
        'ui.router',
        'ui.date'
    ])
    .constant(
    'dataUrl', 'http://localhost:9000/data.json'
)
    .config(function ($stateProvider, $urlRouterProvider) {
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/register");
        //
        // Now set up the states
        $stateProvider
            .state('register', {
                url: "/register",
                templateUrl: "views/register/register.html",
                controller: function ($scope, $state, userData) {
                    $scope.dateOptions = {
                        changeYear: true,
                        changeMonth: true,
                        dateFormat: 'dd-mm-yy'
                    };

                    $scope.form = {};
                    $scope.form.state = 'register';
                    $scope.user = userData.data;

                    $scope.askConfirmation = function (isValid) {
                        // check to make sure the form is completely valid
                        if (isValid) {
                            $state.go('register.confirmation');
                        }
                    };
                },
                resolve: {
                    userData: function ($http, dataUrl) {
                        // $http returns a promise for the url data
                        return $http({method: 'GET', url: dataUrl});
                    }
                }
            })
            .state('register.confirmation', {
                url: "/confirmation",
                templateUrl: "views/register/confirmation.html",
                controller: function ($scope, $state) {
                    $scope.form.state = 'confirmation';
                    $scope.submitConfirm = function () {
                        //DO SOMETHING TO SUBMIT HERE
                        alert('form submitted');
                    };
                    $scope.back = function () {
                        $scope.form.state = 'register';
                        $state.go('register');
                    }
                }
            })
    });
